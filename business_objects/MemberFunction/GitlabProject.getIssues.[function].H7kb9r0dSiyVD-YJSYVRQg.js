function(logger) {
    logger = logger.get('devops.gitlab');
    
    return function(labels, status, ids) {
        logger.debug('getIssues:', labels, status);
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        const queryObj = { projectId:this.project_id };
        
        if(ids) {
            if(!(ids instanceof Array)){
                ids = [ids];
            }
            queryObj.iids = ids;
        }
        else {
            if(labels instanceof Array) {
                labels = labels.join(',');
            }
            queryObj.labels = labels;
            queryObj.state = status || 'opened';
        }
        
        const api = this.getApi();
        return api.Issues.all(queryObj);
    };
    
    
}