function() {
    if(!this._api) {
        if(!this.auth_token) {
            throw `GitlabProject ${this.key} missing auth_token`;
        }
        
        const {Gitlab} = require('gitlab');
        this._api = new Gitlab({token:this.auth_token});
    }
    
    return this._api;
}