function(iss, label) {
    let labels = iss.labels || [];
    if(labels.indexOf(label) < 0) {
        labels.push(label);
        return this.setIssueLabels(iss, labels)
    }
    else {
        return Q(`Issue ${iss.iid} already has label "${label}"`);
    }
}