function(logger) {
    logger = logger.get('devops.gitlab');
    
    return function(name, color, description) {
        logger.debug('createLabel %s %s %s', name, color, description);
        
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        if(!name) {
            throw new Error('label name required');
        }
        
        const api = this.getApi();
        return api.Labels.create(this.project_id, name, color, description);
    }
}