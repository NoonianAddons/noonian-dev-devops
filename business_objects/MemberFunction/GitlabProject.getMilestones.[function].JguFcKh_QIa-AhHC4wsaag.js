function(logger, _) {
    logger = logger.get('devops.gitlab');
    
    return function(status) {
        logger.debug('getMilestones:', status);
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        status = status || 'active';
        
        const api = this.getApi();
        return api.ProjectMilestones.all(this.project_id, {state:status}).then(msList=>{
            msList = _.sortBy(msList, 'start_date');
            return msList;
        });
    };
    
    
}