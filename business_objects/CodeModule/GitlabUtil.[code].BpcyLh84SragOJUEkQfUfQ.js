function(db, Q, _, PromiseUtil) {
    const exports = {};

    const moment = require('moment');
    
    const NEXT_SPRINT_TAG = exports.NEXT_SPRINT_TAG = 'Next Sprint';
    const DEV_DONE_TAG = exports.DEV_DONE_TAG = 'Dev Done';
    
    const MS_DURATION_WEEKS = 2;
    
    const MS_LABEL_PREFIX = exports.MS_LABEL_PREFIX = 'MS_'; 
    const MS_LABEL_COLOR = exports.MS_LABEL_PREFIX = '#FFFFFF';
    const milestoneLabelRegex = new RegExp(MS_LABEL_PREFIX+'(\\d{4}-\\d{2}-\\d{2})'); 
    
    const getMilestoneLabel = function(ms) {
        if(ms && ms.due_date) {
            return MS_LABEL_PREFIX+ms.due_date
        }
        return null;
    };
    
    const isMilestoneLabel = label=>milestoneLabelRegex.test(label);
    
    
    const issueCloseMessage = pkgVersion=>'Incorporated in version '+pkgVersion;
    const msCloseMessage = pkgVersion=>'Closed with version '+pkgVersion;
    
    /**
     * @return list of labels w/ only one MS_ label
     */
    exports.syncIssueMsLabelList = function(labels, ms) {
        let properLabel = getMilestoneLabel(ms);
        let ret = [];
        _.forEach(labels, lab=>{
            if(!isMilestoneLabel(lab)) {
                ret.push(lab);
            }
        });
        if(properLabel) {
            ret.push(properLabel);
        }
        return ret;
    };
    
    
    
    
    const lookupGitlabProject = function(idOrKey) {
        if(idOrKey instanceof db.GitlabProject) {
            return Q(idOrKey);
        }
        
        return db.GitlabProject.findOne({$or:[
            {_id:idOrKey},
            {key:idOrKey},
            {'noonian_package._id':idOrKey},
            {'noonian_package.key':idOrKey}]}
        ).then(gp=>{
            if(!gp) {
                throw 'invalid GitlabProject: '+idOrKey;
            }
            return gp;
        })
    };
    
    
    const lookupIssue = function(projId, issId) {
        
        return lookupGitlabProject(projId).then(gp=>{
            return gp.getIssues(null,null,issId);
        }).then(issList=>issList[0]);
        
    };
    
    /**
     * Pull in issues for next active milestone, tag with NEXT_SPRINT_TAG
     * @param idOrKey - key or id of GitlabProject or BusinessObjectPackage
     */
    exports.beginNextSprint = function(idOrKey) {
        
        return lookupGitlabProject(idOrKey).then(gp=>{
            
            const tagResults = [];
            const tagIssue = function(iss) {
                return gp.addLabelToIssue(iss, NEXT_SPRINT_TAG).then(
                    result=>{tagResults.push(result)},
                    err=>{tagResults.push(err)}
                );
            };
            
            return gp.getNextActiveMilestone()
                .then(gp.getMilestoneIssues.bind(gp))
                .then(issueList=>{
                    
                    let promChain = Q(true);
                    _.forEach(issueList, iss=>{
                        promChain = promChain.then(tagIssue.bind(null, iss));
                    })
                    return promChain;
                })
                .then(()=>{
                    console.log(tagResults);
                    return {message:'Tagged '+tagResults.length+' issues', results:tagResults};
                });
        });
    };
    
    
    /**
     * Bump issues that haven't been closed to the next active milestone
     * @param idOrKey
     * @param prevMs
     */
    const bumpNextSprintTags = 
    exports.bumpNextSprintTags = function(idOrKey, prevMs) {
        
        if(!prevMs || prevMs.state !== 'closed') {
            return Q(false);
        }
        
        return lookupGitlabProject(idOrKey).then(gp=>{
            
            return Q.all([
                gp.getMilestoneIssues(prevMs, 'opened'), 
                gp.getNextActiveMilestone()
            ])
            .spread((prevMsIssues, nextMs)=>{
                console.log('prevIss', prevMsIssues);
                console.log('nextMs', nextMs);
                if(prevMsIssues && prevMsIssues.length && nextMs) {
                    
                    const updateResults = [];
                    const setMs = function(iss) {
                        return gp.setIssueMilestone(iss, nextMs).then(
                            result=>updateResults.push(result),
                            err=>updateResults.push(err)
                        );
                    };
                    
                    let promChain = Q(true);
                    prevMsIssues.forEach(iss=>{
                        promChain = promChain.then(setMs.bind(null, iss));
                    });
                    
                    return promChain.then(()=>{
                        console.log(updateResults);
                        return {message:'Bumped '+updateResults.length+' issues', results:updateResults};
                    });
                }
            })
            ;
        
        });
        
    };
    
    
    /**
     *  1) Close all issues tagged w/ DEV_DONE_TAG
     *  2) Close next active milestone
     *  3) Push all "Next Sprint" tags in the current milestone to the next active one
     */
    exports.sprintComplete = function(idOrKey) {
        
        return lookupGitlabProject(idOrKey).then(gp=>{
            let packageId = gp.noonian_package && gp.noonian_package._id;
            if(!packageId) {
                throw 'Missing noonian_package reference on GitlabProject '+idOrKey;
            }
            
            return db.BusinessObjectPackage.findOne({_id:packageId}).then(bop=>{
                
                const closeResults = [];
                const close = function(iss) {
                    return gp.closeIssue(iss, issueCloseMessage(bop.version)).then(
                        result=>closeResults.push(result),
                        err=>closeResults.push(err)
                    );
                };
                
                return gp.getIssues(DEV_DONE_TAG).then(issueList=>{
                    let promChain = Q(true);
                    _.forEach(issueList, iss=>{
                        promChain = promChain.then(close.bind(null, iss));
                    });
                    promChain = promChain.then(gp.closeNextActiveMilestone.bind(gp, msCloseMessage(bop.version))).then(closedMs=>{
                        return Q.all([
                            {message:'Closed '+closeResults.length+' issues', results:closeResults},
                            bumpNextSprintTags(gp, closedMs)
                        ]).spread((msCloseResult, bumpResults)=>{
                            //TODO fix messages for UI return
                            return {msCloseResult, bumpResults};
                        });
                        
                    });
                    return promChain;
                });
                
            });
        });
    };
    
    //which week is the date in?
    const nthWeekOfMonth = function(d) {
        return Math.floor((d.date()-1)/7) + 1;
    };
    
    const nextMs = function(prevDue, msDuration, msTitleSuffix) {
        let nextStartDate = moment(prevDue).add(1, 'day');
        let nextDueDate = moment(prevDue).add(msDuration, 'week');
        
        let title = nextDueDate.format('MMM YYYY')+' Week '+nthWeekOfMonth(nextDueDate) + msTitleSuffix;
        
        return {
            title,
            due_date:nextDueDate.format('YYYY-MM-DD'),
            start_date:nextStartDate.format('YYYY-MM-DD')
        };
        
    };
    
    exports.getNextMilestonesToGenerate = function(gp) {
        
        var conf = gp.config || {};
        var msDuration = conf.msDuration || MS_DURATION_WEEKS;
        var msTitleSuffix = conf.msTitleSuffix || '';
        var count = conf.generateMilestoneCount || 4;
        
        return gp.getLatestActiveMilestone().then(ms=>{
            const resultArr = [];
            
            let prev = ms;
            
            while(count-- > 0) {
                let next = nextMs(prev.due_date, msDuration, msTitleSuffix);
                resultArr.push(next);
                prev = next;
            }
            
            return resultArr;
        })
    };
    
    exports.generateNextMilestones = function(idOrKey) {
        
        return lookupGitlabProject(idOrKey).then(gp=>{
            var conf = gp.config || {};
            var msDuration = conf.msDuration || MS_DURATION_WEEKS;
            var msTitleSuffix = conf.msTitleSuffix || '';
            var count = conf.generateMilestoneCount || 4;
        
            const resultArr = [];
            const grabResult = r=>resultArr.push(r);
            
            const createMs = function(ms) {
                return gp.createMilestone(ms).then(r=>{
                    var label = getMilestoneLabel(ms);
                    console.log('create', label);
                    return gp.createLabel(label, msLabelColor, ms.title);
                }).then(
                    grabResult,
                    grabResult
                );
            };
                
                
            return gp.getLatestActiveMilestone().then(ms=>{
                let promChain = Q(true);
                
                let latestDue = moment(ms.due_date);
                let prev = ms;
                
                while(count-- > 0) {
                    let next = nextMs(prev.due_date, msDuration, msTitleSuffix);
                    promChain = promChain.then(createMs.bind(null, next));
                    prev = next;
                }
                
                return promChain.then(()=>{
                    return resultArr;
                })
            });
            
        });
    };
    
    
    exports.clearAllIssueMilestones = function(idOrKey) {
        
        return lookupGitlabProject(idOrKey).then(gp=>{
            const resultArr = [];
            const pushResult = r=>resultArr.push(r);
            
            return gp.getIssues().then(issueList=>{
                var promChain = Q(true);
                _.forEach(issueList, iss=>{
                    if(iss.milestone) {
                        promChain = promChain.then(gp.setIssueMilestone.bind(gp, iss, null)).then(
                            pushResult, 
                            pushResult
                        );
                    }
                });
                return promChain.then(()=>resultArr);
            });
        });
    };
    
    
    
    // map MS_ label to milestone object
    const getMilestoneLabelMap = exports.getMilestoneLabelMap = function(idOrKey) {
        
        return lookupGitlabProject(idOrKey).then(gp=>{
            
            return gp.getMilestones().then(msList=>{
                let ret = {};
                _.forEach(msList, ms=>{
                    ret[getMilestoneLabel(ms)] = ms;
                });
                return ret;
            });
            
        });
    };
    
    
    
    const syncIssueMsLabels = 
    exports.syncIssueMsLabels = function(idOrKey, issObjOrId, labelToMilestone) {
        
        var issProm;
        
        if(typeof issObjOrId === 'object') {
            issProm = Q(issObjOrId);
        }
        else {
            issProm = lookupIssue(idOrKey, issObjOrId);
        }
        
        return issProm.then(iss=>{
            
            const ms = iss.milestone;
            const myLabels = iss.labels || [];
            
            const properLabel = getMilestoneLabel(ms);
            
            const nonMsLabels = [];
            const msLabels = [];
            
            myLabels.forEach(lab=>{
                let addTo = isMilestoneLabel(lab) ? msLabels : nonMsLabels;
                addTo.push(lab);
            });
            
            
            var matchingLabel = (msLabels.length === 1 && msLabels[0] === properLabel);
            var properlyNoLabel = (!msLabels.length && !properLabel);
            
            var needsUpdate = !matchingLabel || !properlyNoLabel;
            
            if(needsUpdate) {
                return lookupGitlabProject(idOrKey).then(gp=>{
                    
                    console.log('update needed', iss.id, ms && ms.due_date, msLabels);
                    
                    if(msLabels.length === 1) {
                        //One label that doesn't match issue milestone ->
                        //set milestone to match label
                        console.log('Set milestone issue: %s Milestone: %s', iss.id, msLabels[0]);
                        return (labelToMilestone ? Q(labelToMilestone) : getMilestoneLabelMap(idOrKey)).then(msMap=>{
                            let properMs = msMap[msLabels[0]];
                            console.log(' target MS: '+properMs.title);
                            return gp.setIssueMilestone(iss, properMs);
                        });
                    }
                    else if(properLabel) {
                        //set label to match milestone
                        return gp.setIssueLabels(iss, nonMsLabels.concat(properLabel));
                    }
                    else {
                        //too many milestone labels, no milestone, dunno what to set
                        console.log('Clear milestone labels', iss.id);
                        return gp.setIssueLabels(iss, nonMsLabels);
                    }
                });
            }
            else {
                console.log('No update needed', iss.id, msLabels, nonMsLabels);
                return Q(true);
            }
        });
    };
    
    
    const syncMilestoneLabels = 
    exports.syncMilestoneLabels = function(idOrKey) {
        
        return lookupGitlabProject(idOrKey).then(gp=>{
            var conf = gp.config || {};
            var msLabelColor = conf.msLabelColor || MS_LABEL_COLOR;
            
            return Q.all([
                gp.getMilestones(),
                gp.getLabels()
            ])
            .spread((msList, labelList) => {
                const msLabels = {}; //date -> label obj.
                
                _.forEach(labelList, lab=>{
                    let result = milestoneLabelRegex.exec(lab.name);
                    if(result) {
                        msLabels[result[1]] = lab;
                    }
                });
                
                const createChain = new PromiseUtil.Chain();
                
                //create labels for active milestones that don't have one
                _.forEach(msList, ms=>{
                    console.log(ms);
                    let dd = ms.due_date;
                    
                    if(!msLabels[dd]) {
                        var label = getMilestoneLabel(ms);
                        console.log('create', label);
                        createChain.add(gp.createLabel.bind(gp, label, msLabelColor, ms.title));
                    }
                    delete msLabels[dd];
                });
                
                //Remove MS_ labels that don't have an active milestone 
                Object.values(msLabels).forEach(lab=>{
                    console.log('delete %s', lab);
                    createChain.add(gp.deleteLabel.bind(gp, lab.id));    
                });
                
                return createChain.promise.then(()=>{
                    return Q.all([
                        gp.getIssues(),
                        getMilestoneLabelMap(idOrKey)
                    ]);
                })
                .spread((issueList,labelToMilestone)=>{
                    
                    console.log('Checking '+issueList.length+' issues');
                    
                    const updateChain = new PromiseUtil.Chain();
                    
                    _.forEach(issueList, iss=>{
                        updateChain.add(syncIssueMsLabels.bind(null, idOrKey, iss, labelToMilestone))
                    });
                    
                    return updateChain.promise;
                    
                });
            });
        
        });
        
    };
    
    
    
    return exports;
}