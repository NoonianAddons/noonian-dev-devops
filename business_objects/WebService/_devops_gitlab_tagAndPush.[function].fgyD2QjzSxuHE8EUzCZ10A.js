function(db, Q, _, config, queryParams) {
    // console.log(queryParams);
    const gpId = queryParams.id;
    
    if(!gpId) {
        throw 'missing required params';
    }
    
    return db.GitlabProject.findOne({_id:gpId}).then(gp=>{
        
        if(!gp) {
            throw 'Invalid GitlabProject id '+gpId;
        }
        
        const bopId = _.get(gp, 'noonian_package._id');
        if(!bopId){
            throw 'Missing Noonian Package reference';
        }
        
        const serverConf = config.serverConf;
        
        const simpleGit = require('simple-git');
        const semver = require('semver');
        const util = require('util');
        const fs = require('fs');
        const path = require('path');
        
        const git = simpleGit(serverConf.instanceDir).outputHandler((command, stdout, stderr) => {
            stdout.pipe(process.stdout);
            stderr.pipe(process.stderr);
        });
         
        const wrap = function(fName) {
            return util.promisify(git[fName]).bind(git);
        };
        
        const getTags = wrap('tags');
        const addFiles = wrap('add');
        const rmFiles = wrap('rm'); 
        const commit = wrap('commit');
        const tag = wrap('addAnnotatedTag');
        const push = wrap('push'); 
        const pushTags = wrap('pushTags'); 
        
        return db.BusinessObjectPackage.findOne({_id:bopId}).then(bop=>{
            var msg = 'Dist package: v'+bop.version;
            
            const packageFilename = `${bop.key}.${bop.version}.json`;
            
            return getTags().then(tags=>{
                if(tags.latest === bop.version) {
                    throw 'Latest tag already matches '+bop.version;
                }
                
                //First add the file and pkg meta
                
                
                return addFiles([
                    'dist/'+packageFilename,
                    bop.key+'/pkg_meta.json'
                ]);
                
            })
            .then(()=>{
                //Remove dist packages that are not of this version
                const distDir = path.resolve(serverConf.instanceDir, 'dist');
                const toRemove = [];
                const match = new RegExp('^'+bop.key);
                fs.readdirSync(distDir).forEach(fileName=>{
                    if(fileName !== packageFilename && match.test(fileName)) {
                        toRemove.push(rmFiles(['dist/'+fileName]));
                    }
                });
                
                //In case some packages weren't in the index, allow for some errors here
                return Q.allSettled(toRemove);
            
            })
            .then(()=>commit(msg))
            .then(()=>tag(bop.version, msg))
            .then(()=>push('origin'))
            .then(()=>pushTags('origin'))
            .then(()=>{
                return {message:'Pushed!'};
            })
            ;
            
        
        });
    });
}