function(queryParams, GitlabUtil) {
    const id = queryParams.id;
    
    if(!id) {
        throw 'missing required params';
    }
    
    return GitlabUtil.generateNextMilestones(id).then(msResults=>{
        return {
            msResults, 
            message:'success'
        };
        // return GitlabUtil.syncMilestoneLabels(id).then(labelResults=>{
        //     return {
        //         msResults, 
        //         labelResults,
        //         message:'success'
        //     }
        // })
    });
    
}