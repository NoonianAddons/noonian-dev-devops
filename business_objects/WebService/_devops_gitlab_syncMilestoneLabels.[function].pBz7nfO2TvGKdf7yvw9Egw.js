function(queryParams, GitlabUtil) {
    const id = queryParams.id;
    
    if(!id) {
        throw 'missing required params';
    }
    
    return GitlabUtil.syncMilestoneLabels(id).then(labelResults=>{
        return {
            labelResults,
            message:'success'
        }
    });
    
}